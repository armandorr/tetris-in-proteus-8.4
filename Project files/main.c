#include <xc.h>
#define _XTAL_FREQ 8000000  
#include "config.h"
#include "GLCD.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define TMR0ON T0CONbits.TMR0ON

char buff[64];			//Buffer per escriure per pantalla
int matrix[8][16]; 		//Matriu d'ocupats
int piece_pos[4][2];		//Identificador de la pe�a
int actual_rot;			//Rotacio actual de la pe�a
int letter;			//Valor llegit pel terminal
int letter_to_read = 0;		//Ens indica si hi ha una lletra per llegir
int baixar = 0;			//Flag que ens indica si ha pasat el temps del timer i cal baixar
int score = 0;			//Puntuacio del joc
int mode = 0;			//Mode de joc
int power_value1 = 0;		//Valor de la conversi� del potenciometre
int power_value2 = -1;		//Auxiliar pel valor de la conversio del potenciometre
int timer_game;			//Timer actual del joc


//Texts per escriure per pantalla al principi i al final
const char * start = "TETRIS on a GLCD\n";
const char * easy_mode = "Press(a) to easy mode\n";
const char * normal_mode = "Press(s) to normal mode\n";
const char * game_over = "GAME OVER\n";
const char * on_easy = "on easy mode\n";
const char * on_normal = "on normal mode\n";
const char * press_restart = "Press(r) to restart\n";

//Inicialitza la matriu d'ocupats amb cap ocupat excepte la zona de puntuacio
void reset_matrix(){
   for(int i = 0; i < 8; ++i){
      for(int j = 0; j < 16; ++j){
	 if(i == 0 && (j == 14 || j == 15)) matrix[i][j] = 1;
	 else matrix[i][j] = 0;   
      }
   }
}

//Configuracio del pic i la glcd
void configPIC_i_GLCD(){
   ANSELB = 0x00;		//Configurem B digital
   ANSELC = 0x00;		//Configurem C digital
   ANSELD = 0x00;		//Configurem D digital
   TRISB = 0x00;		//Configurem B de sortida
   TRISC = 0xFF;		//Configurem C de entrada
   TRISD = 0x00;		//Configurem D de sortida
   PORTB = 0;			//Inicialitzem el port B
   PORTD = 0; 			//Inicialitzem el port C
   
   //Tractament de les interrupcions en general
   RCONbits.IPEN = 1;
   INTCONbits.GIEH = 1;
   INTCONbits.GIEL = 1;
   
   //Tractament de les interrupcions provocades pel timer
   INTCONbits.TMR0IE = 1;
   INTCON2bits.TMR0IP = 1;
   INTCONbits.TMR0IF = 0;
   T0CON = 06; 			//PRE: 128
   TMR0ON = 0;
   
   //Tractament de les interrupcions provocades pel virtual terminal
   PIE1bits.RC1IE = 1;
   IPR1bits.RC1IP = 0;
   PIR1bits.RC1IF = 0;
   
   //Configuracio del virtual terminal
   RCSTA1bits.CREN = 1;
   TXSTA1bits.SYNC = 0;
   RCSTA1bits.SPEN = 1;
   BAUDCON1bits.BRG16 = 1;
   TXSTA1bits.BRGH = 1;
   SPBRG1 = 16;
   
   //Configuracio del potenciometre
   ADCON0 = 0x01; 		//Activem el AD i escollim AN0
   ADCON1 = 0x00; 		//Hihg = Vdd = 5V, Low = Vss = 0V
   ADCON2 = 0x89; 		//Right justified, 2*TAD, Conversion clock = Fosc/8
   
   
   GLCDinit();		   	//Inicialitzem la pantalla
   clearGLCD(0,7,0,127);     	//Esborrem la pantalla
   setStartLine(0);          	//Definim linia d'inici
   
   reset_matrix(); 		//Inicialitzar matriu d'ocupats
}

//Funcio per escriure un text en una posicio en concret
void writeText(byte page, byte y, char * s){ 
   int i = 0;
   while (*s!='\n' && *s!='\0'){
      putchGLCD(page, y+i, *(s++));
      ++i;
   }
}

//Escriu per pantalla la puntuacio (amunt a la detra)
void write_score(){
   sprintf(buff, "%d", score);
   writeText(0, 23, buff);
}

//Escriu per pantalla el missatge de la velocitat triada
void write_potentiometre(){
   sprintf(buff, "      "); 		//Esborrar el anterior valor ja que potser al modificarlo no sobreescrivim algunes posicions
   writeText(6, 20, buff);
   int power = power_value1/102;	//Rang de velocitats entre 0 i 10
   if(power == 10) sprintf(buff, "Selected velocity: Max", power);
   else if(power == 0) sprintf(buff, "Selected velocity: Min", power);
   else sprintf(buff, "Selected velocity: %d", power);
   writeText(6, 2, buff);
}

//Funcio que ens indica si una posici� esta marcada com ocupada a la matriu d'ocupats
//Si es una posici� erronea tamb� ens ho indica com si estigues ocupada, aixi no sortirem dels marges
int visited(int x, int y){
   if (x >= 0 && y >= 0 && x<8 && y<16) return matrix[x][y];
   else return 1;
}

//Dibuixa el rectangle de la puntuaci� (adalt a la dreta)
void print_rectangle(){
   for(int i = 0; i < 7; ++i) SetDot(i,112);
   for(int j = 112; j < 128; ++j) SetDot(7,j);
}

//Dibuixa un cuadrat desde la posici� x,y
void paint_square(int x, int y){
   for(int i = x; i < x + 7; ++i){
      for(int j = y; j < y + 7; ++j){
	 if ((i!=x && (j!=y || j!=y+8)) || (i!=x+8 && (j!=y || j!=y+8))) SetDot(i,j);
      }
   }
}

//Esborra el cuadrat de la posicio x,y
void erase_square(int x, int y){
   for(int i = x; i < x+7; ++i){
      for(int j = y; j < y+7; ++j){
	 if ((i!=x && (j!=y || j!=y+8)) || (i!=x+8 && (j!=y || j!=y+8))) ClearDot(i,j);
      }
   }
}

//Dibuixa la pe�a sencera
void paint_piece(){
   paint_square(piece_pos[0][0]*8,piece_pos[0][1]*8);
   paint_square(piece_pos[1][0]*8,piece_pos[1][1]*8);
   paint_square(piece_pos[2][0]*8,piece_pos[2][1]*8);
   paint_square(piece_pos[3][0]*8,piece_pos[3][1]*8);
}

//Esborra la pe�a sencera
void erase_piece(){
   erase_square(piece_pos[0][0]*8,piece_pos[0][1]*8);
   erase_square(piece_pos[1][0]*8,piece_pos[1][1]*8);
   erase_square(piece_pos[2][0]*8,piece_pos[2][1]*8);
   erase_square(piece_pos[3][0]*8,piece_pos[3][1]*8);
}

//Mou la pe�a cap a la dreta si pot
void right(){
   if(visited(piece_pos[0][0], piece_pos[0][1] + 1)) return;
   if(visited(piece_pos[1][0], piece_pos[1][1] + 1)) return;
   if(visited(piece_pos[2][0], piece_pos[2][1] + 1)) return;
   if(visited(piece_pos[3][0], piece_pos[3][1] + 1)) return;
   erase_piece();
   ++piece_pos[0][1];
   ++piece_pos[1][1];
   ++piece_pos[2][1];
   ++piece_pos[3][1];
   paint_piece();
}

//Mou la pe�a cap a l'esquerra si pot
void left(){
   if(visited(piece_pos[0][0], piece_pos[0][1] - 1)) return;
   if(visited(piece_pos[1][0], piece_pos[1][1] - 1)) return;
   if(visited(piece_pos[2][0], piece_pos[2][1] - 1)) return;
   if(visited(piece_pos[3][0], piece_pos[3][1] - 1)) return;
   erase_piece();
   --piece_pos[0][1];
   --piece_pos[1][1];
   --piece_pos[2][1];
   --piece_pos[3][1];
   paint_piece();
}

//Ens indica si la pe�a pot baixar m�s encara
int can_go_down(){
   if(visited(piece_pos[0][0] + 1, piece_pos[0][1])) return 0;
   if(visited(piece_pos[1][0] + 1, piece_pos[1][1])) return 0;
   if(visited(piece_pos[2][0] + 1, piece_pos[2][1])) return 0;
   if(visited(piece_pos[3][0] + 1, piece_pos[3][1])) return 0;
   return 1;
}

//Mou la pe�a cap avall si pot
void down(){
   if(can_go_down){
      erase_piece();
      ++piece_pos[0][0];
      ++piece_pos[1][0];
      ++piece_pos[2][0];
      ++piece_pos[3][0];
      paint_piece();
   }
}

//Rota la pe�a amb els parametres d'entrada (la posicio 1 de la pe�a, per ser el centre de gir, mai rota)
//Si no pot rotar perque entra amb conflicte amb altres pe�es doncs no ho fa
void rotation(int a, int b, int c, int d, int e, int f){
   if(visited(piece_pos[0][0] + a, piece_pos[0][1] + b)) return;
   if(visited(piece_pos[2][0] + c, piece_pos[2][1] + d)) return;
   if(visited(piece_pos[3][0] + e, piece_pos[3][1] + f)) return;
   erase_piece();
   piece_pos[0][0] += a;
   piece_pos[0][1] += b;
   piece_pos[2][0] += c;
   piece_pos[2][1] += d;
   piece_pos[3][0] += e;
   piece_pos[3][1] += f;
   if(++actual_rot == 4) actual_rot = 0;
   paint_piece();
}

//Amb la pe�a que tinguem i la rotacio actual escollim la rotacio que li toca fer (el cuadrat ni te cas propi, no fa res)
void rotate_piece(int target){
   if (target == 0){ 	//Pe�a barra
      if(actual_rot == 0) rotation(-1,1,1,-1,2,-2);
      else if(actual_rot == 1) rotation(1,1,-1,-1,-2,-2);
      else if(actual_rot == 2) rotation(1,-1,-1,1,-2,2);
      else rotation(-1,-1,1,1,2,2);
   }
   else if (target == 1){ //Pe�a L
      if(actual_rot == 0) rotation(-1,1,1,-1,2,0); 
      else if(actual_rot == 1) rotation(1,1,-1,-1,0,-2); 
      else if(actual_rot == 2) rotation(1,-1,-1,1,-2,0); 
      else rotation(-1,-1,1,1,0,2);
   }
   else if (target == 2){ //Pe�a J
      if(actual_rot == 0) rotation(1,-1,-1,1,0,2);
      else if(actual_rot == 1) rotation(-1,-1,1,1,2,0); 
      else if(actual_rot == 2) rotation(-1,1,1,-1,0,-2);
      else rotation(1,1,-1,-1,-2,0);
   }
   else if (target == 3){ //Pe�a T
      if(actual_rot == 0) rotation(-1,1,1,1,1,-1); 
      else if(actual_rot == 1) rotation(1,1,1,-1,-1,-1); 
      else if(actual_rot == 2) rotation(1,-1,-1,-1,-1,1);
      else rotation(-1,-1,-1,1,1,1);
   }
   else if (target == 4){ //Pe�a S
      if(actual_rot == 0) rotation(1,-1,-1,-1,-2,0); 
      else if(actual_rot == 1) rotation(-1,-1,-1,1,0,2);
      else if(actual_rot == 2) rotation(-1,1,1,1,2,0); 
      else rotation(1,1,1,-1,0,-2);
   }
   else if (target == 5){ //Pe�a Z
      if(actual_rot == 0) rotation(-1,1,-1,-1,0,-2); 
      else if(actual_rot == 1) rotation(1,1,-1,1,-2,0);
      else if(actual_rot == 2) rotation(1,-1,1,1,0,2); 
      else rotation(-1,-1,1,-1,2,0);
   }
}

//Posa a la matriu d'ocupats la pe�a actual(es crida quan la pe�a no pot baixar m�s)
void fill(){
   matrix[piece_pos[0][0]][piece_pos[0][1]] = 1;
   matrix[piece_pos[1][0]][piece_pos[1][1]] = 1;
   matrix[piece_pos[2][0]][piece_pos[2][1]] = 1;
   matrix[piece_pos[3][0]][piece_pos[3][1]] = 1;
}

//Esborra una linea completa
void erase_line(int n){
   for(int j = 0; j < 16; ++j){
      matrix[n][j] = 0;
      erase_square(n*8,j*8);
   }
}

//Baixa totes les pe�es 1 cop desde la fila n cap amunt (fila n sera la fila esborrada)
void gravity(int n){
   for(int i = n; i >= 0; --i){
      for(int j = 0; j < 16; ++j){
	 if((i != 0 || (j != 14 && j != 15)) && matrix[i][j]){ //no tocar cuadre de puntuacio i que hi hagi pe�a per baixar
	    if(!visited(i+1,j)){ //mirem si pot baixar el quadrat i el baixem
	       matrix[i][j] = 0;
	       erase_square(i*8,j*8);
	       matrix[i+1][j] = 1;
	       paint_square((i+1)*8,j*8);
	    }
	 }
      }
   }
}

//Comproba si hi ha alguna linea completa, la esborra i fa caure les de adalt
void check_line(){
   fill();
   INTCONbits.TMR0IE = 0;
   TMR0ON = 0;
   for(int i = 7; i > 0; --i){
      int completed = 1;
      for(int j = 0; j < 16 && completed; ++j){ if(!matrix[i][j]) completed = 0;}
      if(completed){ //Si esta completa l'esborrem, augmentem score i baixem les pe�es de dalt
	 erase_line(i);
	 ++score;
	 write_score();
	 gravity(i);
	 ++i; //Sumem a i per que torni a mirar a la mateixa posici� per si la fila que hem baixat tamb� esta completa
      }
   }
   PIR1bits.RC1IF = 0;
   letter_to_read = 0;
   INTCONbits.TMR0IF = 0;
   baixar = 0;
   TMR0 = timer_game;	
   INTCONbits.TMR0IE = 1;
   if(mode) TMR0ON = 1;
}

//Funcio per quan hem perdut, treu missatges i puntuacio final
//Es queda en espera fins que es prem r per fer un reset del joc, podem tornar a escollir la velocitat de joc (pero no el mode)
void gameover(){
   INTCONbits.TMR0IE = 0;
   TMR0ON = 0;
   clearGLCD(0,7,0,127);
   writeText(1,8,game_over);
   if(mode) writeText(2,6,on_normal);
   else writeText(2,7,on_easy);
   writeText(3,4,press_restart);
   sprintf(buff,"Final score: %d", score);
   writeText(5,5,buff);
   int stop = 0;
   power_value2 = -1;
   while(!stop){
      if(mode){
	 ADCON0bits.GO = 1;  // Demanem una conversi�
	 while(ADCON0bits.GO){}
	 power_value1 = 1023-ADRESH*256+ADRESL;  //Valor de la conversi� (entre 0 i 1023)(resta feta per "invertir" el valor)
	 if(power_value1 != power_value2){
	    power_value2 = power_value1;
	    write_potentiometre();
	 }
      }
      if(letter_to_read){
	 if(letter == 114) stop = 1; //Si llegim r(restart) sortim del bucle
	 letter_to_read = 0;
      }
   }
   clearGLCD(0,7,0,127);
   score = 0;
   write_score();
   reset_matrix();
   if(mode) timer_game = (15,258789)*power_value1 + 34286; //Conversio per l'interval entre 1s i 2s
   	
   PIR1bits.RC1IF = 0;
   letter_to_read = 0;
   INTCONbits.TMR0IF = 0;
   baixar = 0;
   TMR0 = timer_game;	
   INTCONbits.TMR0IE = 1;
   if(mode) TMR0ON = 1;
}

//Inicialitzem la posici� de la pe�a, retorna si alguna de les posicions on tocaba anar la pe�a esta ocupada(condicio per acabar la partida)
int init_piece(int a, int b, int c, int d, int e, int f, int g, int h){
   piece_pos[0][0] = a;
   piece_pos[0][1] = b;
   piece_pos[1][0] = c;
   piece_pos[1][1] = d;
   piece_pos[2][0] = e;
   piece_pos[2][1] = f;
   piece_pos[3][0] = g;
   piece_pos[3][1] = h;
   return matrix[a][b] || matrix[c][d] || matrix[e][f] || matrix[g][h];
}

//Escollim pe�a a trav�s d'un target que sera random
//Retorna si la pe�a no pot ser contruida ja que les posicions estan ocupades
//Si aixo passa haurem perdut la partida
int choose_piece(int target){
   int finish = 0;
   if(target == 0) finish = init_piece(0,6,0,7,0,8,0,9);      //Pe�a 1 (barra)
   else if(target == 1) finish = init_piece(1,6,1,7,1,8,0,8); //Pe�a L
   else if(target == 2) finish = init_piece(1,8,1,7,1,6,0,6); //Pe�a J
   else if(target == 3) finish = init_piece(1,6,1,7,0,7,1,8); //Pe�a T
   else if(target == 4) finish = init_piece(0,8,0,7,1,7,1,6); //Pe�a S
   else if(target == 5) finish = init_piece(0,6,0,7,1,7,1,8); //Pe�a Z
   else finish = init_piece(0,7,1,7,0,8,1,8);                 //Pe�a 0 (cuadrat)
   return finish;
}

//Tractament de la interrupcio de alta prioritat del timer
void interrupt HP(void){
   if(INTCONbits.TMR0IE == 1 && INTCONbits.TMR0IF == 1){
      baixar = 1;
      TMR0 = timer_game;		
      INTCONbits.TMR0IF = 0;
   }
}

//Tractament de la interrupcio de baixa prioritat del virtual terminal
void interrupt low_priority LP(void){
   if(PIE1bits.RC1IE == 1 && PIR1bits.RC1IF == 1){
      letter = RCREG1;
      letter_to_read = 1;
      PIR1bits.RC1IF = 0;
   }
}

void main(void){
	configPIC_i_GLCD(); //Configurem la placa i la glcd
	writeText(2,4,start);
	writeText(4,2,easy_mode);
	writeText(5,1,normal_mode);	//Escrivim els missatges d'inici
	int stop = 0;
	double seed = 0;
	while(!stop){ //Esperem fins llegir a o s pel mode facil o normal respectivament
		ADCON0bits.GO = 1; // Demanem una conversi�
		while(ADCON0bits.GO){}
		power_value1 = 1023-(ADRESH*256+ADRESL); //Valor de la conversio entre 0 i 1023(restem per obtenir el valor "invertit")
		if(power_value1 != power_value2){
			power_value2 = power_value1;
			write_potentiometre();
		}
		if(letter_to_read){ //Seleccionem mode de joc, 0 mode facil, 1 mode normal
			if(letter == 97){stop = 1; mode = 0;}
			if(letter == 115){stop = 1; mode = 1;}
			letter_to_read = 0;
		}
		++seed; //L'usarem per triar una seed random cada partida, dependra del temps d'espera a la pantalla d'inici modul 10^5
		if(seed == 100000) seed = 0; //Per evitar overflow innecessari i problemes
	}
	timer_game = (15625/1024)*power_value1 + 34286; //Conversio per l'interval entre 1s i 2s
	TMR0 = timer_game;
	srand(seed);
	clearGLCD(0,7,0,127);
	write_score();
	while(1){
		print_rectangle();
		int random = rand()%7;
		int finish = choose_piece(random);
		paint_piece();
		if(!finish){
			actual_rot = 0;
			int sigue = 1;
			while(sigue){
				if(mode) TMR0ON = 1; //Si ens trobem al mode normal activem el timer
				if(RCSTA1bits.OERR){ //Overrrun problemes
					RCSTA1bits.CREN = 0;
					RCSTA1bits.SPEN = 0;
					RCSTA1bits.SPEN = 1;
					RCSTA1bits.CREN = 1;
					letter_to_read = 0;
				}
				if(baixar){down(); baixar = 0;}
				else if (letter_to_read){
					if(letter == 97) left(); 			//a
					else if(letter == 100) right(); 		        //d
					else if(letter == 119) rotate_piece(random);     //w
					else if(letter == 115) down(); 		        //s (sempre pots baixar una fila clicant a s)
					letter_to_read = 0;
				}
				sigue = can_go_down();
				if(!sigue && mode){	//serveix per poder fer un o diversos moviments al final
					TMR0 = 0xE796; //0.4s, temps per fer un possible ultim moviment
					INTCONbits.TMR0IF = 0;
					baixar = 0;
					while(!baixar){
						if (letter_to_read){
							if(letter == 97) left(); 			//a
							else if(letter == 100) right(); 		//d	    
							letter_to_read = 0;
						}
					}
					TMR0 = timer_game;		
					sigue = can_go_down();
					INTCONbits.TMR0IF = 0;
					if(sigue) baixar = 0;
				}
			}
			check_line();
		}
		else{
			__delay_ms(1000); 		//temps per mirar el final de la partida
			gameover();			//funcio de final de partida
		}
	}
}
